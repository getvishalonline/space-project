# Project Solar

## Technologies

- Sinatra Micro Web Framework
- NextJs
- MongoDB


## Scope

Scope of this project is to calculate accuracy of pi and find the circumference of Sun.

### Approach

Backend Application: 

This is a api driven application this has 2 endpoints

```
GET "/api/v1/get_info"

POST "/api/v1/sun_circumference"
```

The Radius of sun is set to  = 696340

For finding π accuracy i used Spigot Algorithm by using this algorithm i manged store all calculation to our Persistant data store. this helps to speed up the process so whenever new request comes in we just pass the previous calculation to Spigot library, Hence it only has to calculate once.

> To see Spigot library go to backend/lib/spigot.rb

since bigdecimal/long int has a limit. I
m just saving the calculations results, π, circumference as string. This may not be a optimal solution as there will be a cost when value holds big numbers. **I did a stress test with an infite loop so i didn't faced any issue until i manually closed it**

![Stress test Result](stress-test.png)

### Database Scheme

```mongodb
  field :current_pi, default: ""
  field :nn, type: String
  field :nr, type: String
  field :q, type: String  
  field :r, type: String
  field :t, type: String 
  field :k, type: String
  field :n, type: String
  field :l, type: String
  field :circumference, type: String
```
### Formulas

*Finding Circumference of Sun*

Considering Sun in a Spherical shape.

```
circumference = 2πr
```

*Spigot (Alogorithm to find π accuracy)*

A spigot algorithm is an algorithm for computing the value of a transcendental number (such as π or e) that generates the digits of the number sequentially from left to right providing increasing precision as the algorithm proceeds. Spigot algorithms also aim to minimize the amount of intermediate storage required. The name comes from the sense of the word "spigot" for a tap or valve controlling the flow of a liquid. Spigot algorithms can be contrasted with algorithms that store and process complete numbers to produce successively more accurate approximations to the desired transcendental.


## Expected System Design

- Dockerise Backend and use Google Cloud Run or K8 for hosting it.

[Expected System Design](expected-sd.drawio)

## Improvement in Existing system

- Move completely to serverless architecture.
- seperating the calculation unique to the user so each user will have their own session.

## Future Enhancements

- if we are planning to store log trail of each request then prefer to use casandra over mongodb.

